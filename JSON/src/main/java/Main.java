import java.io.File;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        File json = new File("D:\\IdeaProjects\\JSON\\src\\main\\resources\\computerJSON.json");
        File schema = new File("D:\\IdeaProjects\\JSON\\src\\main\\resources\\computerJSONScheme.json");

        JSONParser parser = new JSONParser();
        printList(parser.getComputerList(json));
    }

    private static void printList(List<Computer> computers) {
        System.out.println("JSON");
        for (Computer computer : computers) {
            System.out.println(computer);
        }
    }
}
