import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONParser {
    private ObjectMapper objectMapper;

    public JSONParser() {
        this.objectMapper = new ObjectMapper();
    }

    public List<Computer> getComputerList(File jsonFile){
        Computer[] computers = new Computer[0];
//        List<Computer> computers = new ArrayList<>();
        try{
            computers = objectMapper.readValue(jsonFile, Computer[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Arrays.asList(computers);
    }
}